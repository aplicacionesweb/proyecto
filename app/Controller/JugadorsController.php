<?php

class JugadorsController extends AppController {

    public $helpers = array('Html', 'Form', 'Js');
    public $name = 'Jugadors';
    public $components = array('Session', 'Utilidades', 'RequestHandler');
    public $uses = array('Pareja', 'Jugador');

    /* //Garantizar que solo los administradores puedan registrar. 
      public function isAuthorized($user) {
      //if (in_array($this->action, array('registrar', 'delete'))) {
      if ($this->action =='registrar' and $user['tipo'] === 'Administrador') {
      return true;
      }
      return parent::isAuthorized($user);
      } */

    function index() {
        //$this->set('datos', $this->Jugador->find('all', array('fields' => array('_id'))));
    }

    //funcion asociada con una vista que permite registrar jugadores
    function registrar() {
        //pr($this->request->data);
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                /* si el view usó ajax los datos enviados se dañan, se usa organizarDatos para
                  restaurar los datos a la forma adecuada */
                $this->_organizarDatos();

                $datosTorneo = array(
                    'categoria' => $this->request->data['Jugador']['categoria'],
                    'modalidad' => 'Solo',
                    'gran_torneo' => $this->request->data['Jugador']['gran_torneo'],
                    'genero' => $this->request->data['Jugador']['genero']
                );

                //se eliminan los datos que no pertenecen al participante
                unset($this->request->data['Jugador']['gran_torneo']);

                //se agrega el ranking, para jugadores nuevos es 0
                $this->request->data['Jugador']['ranking'] = 0;

                if ($this->Utilidades->torneoAdmiteInscripcion($datosTorneo['gran_torneo'])) {

                    $idJugador = $this->request->data['Jugador']['_id'];

                    if (!$this->Utilidades->estaRegistrado($datosTorneo['gran_torneo'], $idJugador)) {
                        if ($idJugador != '') {//el jugador ya existe
                            $this->Jugador->id = $idJugador;
                            if ($this->Jugador->saveField('categoria', $this->request->data['Jugador']['categoria'])) {//se actualiza la categoria del jugador
                                $this->Utilidades->guardarTorneo($datosTorneo, $idJugador); //se registra el jugador en el torneo
                                $this->Utilidades->actualizarCantidadJugadores($datosTorneo['gran_torneo'],1); //se actualiza la cantidad de jugadores inscritos al grantorneo
                                $this->Session->setFlash('El jugador ha sido registrado.');
                                $this->request->data = array();//se borran los datos par que el formulario se limpie
                            } else {
                                $this->Session->setFlash('Ha ocurrido un error, el jugador no se ha registrado.');
                            }
                        } else {//el jugador aun no existe
                            $this->Jugador->Behaviors->attach('Mongodb.SqlCompatible'); //para que funcione isUnique
                            if ($this->Jugador->save($this->request->data)) { //se guarda el jugador
                                $idJugador = $this->Jugador->find('all', array('conditions' => array('identificacion' => $this->request->data['Jugador']['identificacion']),
                                    'fields' => array('_id'))); //se obtiene el _id del jugador recien guardado
                                $this->Utilidades->guardarTorneo($datosTorneo, $idJugador[0]['Jugador']['_id']); //se registra el jugador en el torneo
                                $this->Utilidades->actualizarCantidadJugadores($datosTorneo['gran_torneo'],1); //se actualiza la cantidad de jugadores inscritos al grantorneo
                                $this->Session->setFlash('El jugador a sido registrado.');
                                $this->request->data = array();//se borran los datos par que el formulario se limpie
                            } else {
                                $this->Session->setFlash('Ha ocurrido un error, el jugador no se ha registrado.');
                            }
                        }
                    } else {
                        $this->Session->setFlash('El Jugador actualmente está inscrito en este torneo, NO se puede registrar nuevamente');
                    }
                } else {
                    $this->Session->setFlash('Este torneo no admite más inscripciones.');
                }
            }
            //como al terminar la llamada por post se regresa a registrar(independientemente si hay error o si todo salio bien)
            //se debe de pasar a la vista la variable con los nombres de los grantorneo
            $this->set('nombreTorneos', $this->Utilidades->getNombresGranTorneo('creado'));
            $this->set('categorias', $this->Utilidades->getCategorias(''));
        } else {//es llamada por get, usado cuando se despliega la vista
            $nombreTorneos = $this->Utilidades->getNombresGranTorneo('creado');
            if(empty ($nombreTorneos)){
                $this->Session->setFlash('No hay Torneos disponibles para realizar el registro');
            }
            $this->set('nombreTorneos', $nombreTorneos);
            $this->set('categorias', $this->Utilidades->getCategorias(''));
        }
    }


    //funcion llamada por ajax
    function getDatosParticipante() {
        $datos = $this->Jugador->find('all', array('conditions' => array('identificacion' => $this->request->data['Jugador']['identificacion'])));
        if (!empty($datos)) {
            $this->set('id', $datos[0]['Jugador']['_id']);
            $this->set('nombres', $datos[0]['Jugador']['nombres']);
            $this->set('apellidos', $datos[0]['Jugador']['apellidos']);
            $this->set('fecha_nacimiento', $datos[0]['Jugador']['fecha_nacimiento']);
            $this->set('genero', $datos[0]['Jugador']['genero']);
            $categoria = $datos[0]['Jugador']['categoria'];
            $this->set('categoria', $categoria);
            $this->set('categorias', $this->Utilidades->getCategorias($categoria));
        } else {
            $this->set('id', '');
            $this->set('nombres', '');
            $this->set('apellidos', '');
            $this->set('fecha_nacimiento', '');
            $this->set('genero', '');
            $this->set('categoria', '');
            $this->set('categorias', $this->Utilidades->getCategorias(''));
        }
        $this->render('registro_jugador', 'ajax');
    }

    function _organizarDatos() {
        if (count($this->request->data['Jugador']) == 2) {
            $granTorneo = $this->request->data['Jugador']['gran_torneo'];
            $ide = $this->request->data['Jugador']['identificacion'];
            foreach ($this->request->data as $key => $value) {
                $this->request->data['Jugador'][$key] = $value;
                unset($this->request->data[$key]);
            }
            $this->request->data['Jugador']['gran_torneo'] = $granTorneo;
            $this->request->data['Jugador']['identificacion'] = $ide;
            //pr($this->request->data);
        }
    }
    
    //funcion asociada con una vista que permite listar todos los jugadodores
    function listar(){
        if($this->RequestHandler->isAjax()){
            $valor = $this->request->data['Jugador']['valor'];
            $opcion = $this->request->data['Jugador']['opcion'];
            $this->Jugador->Behaviors->attach('Mongodb.SqlCompatible');
            if($opcion == '1'){//identificacion
                $this->set('jugadores', $this->Jugador->find('all', array('conditions' => array('identificacion LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '2'){//nombres
                $this->set('jugadores', $this->Jugador->find('all', array('conditions' => array('nombres LIKE' => '%'.$valor.'%'))));
            }else if($opcion == '3'){//apellidos
                $this->set('jugadores', $this->Jugador->find('all', array('conditions' => array('apellidos LIKE' => '%'.$valor.'%'))));
            }
            $this->render('lista', 'ajax');
        }else{
            $this->set('jugadores', $this->Jugador->find('all'));
        }
    }
    
    //funciona asociada a una vista que permite mostrar los datos detallados de un jugador
    function detalle($id = null){
        $this->Jugador->id = $id;
        $this->set('jugador', $this->Jugador->read());
    }
    
    //funcion asociada a una vista que permite editar los datos un jugador
    function editar($id = null){
        $this->Jugador->id = $id;
         if ($this->request->is('get')) {
            $this->request->data = $this->Jugador->read();
         } else {
            if ($this->Jugador->save($this->request->data)) {
               $this->Session->setFlash('El Jugador ha sido actualizado.');
               $this->redirect(array('action' => 'listar'));
            }
         }
    }
    
    //funcion asociada a una vista que permite eliminar un jugador
    /*function eliminar($id){
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
         }
         if ($this->Jugador->delete($id)) {
            $this->Session->setFlash('El Jugador ha sido eliminado.');
            //eliminar de todos los torneos que exista
            $this->redirect(array('action' => 'listar'));
         }
    }*/
    
    function rankings(){
        if ($this->RequestHandler->isAjax()) {
            $this->ranking($this->request->data['Jugador']['modalidad']);
            $this->render('ranking', 'ajax');
        } else {
            $this->ranking('Masculino');
        }        
    }
    
    function ranking($genero){
        if($genero == 'mixto'){
            //$this->Pareja->Behaviors->attach('Mongodb.SqlCompatible');
            $jugadores = $this->Pareja->find('all', array(
                'fields' => array('nombresJ1', 'apellidosJ1','nombresJ2', 'apellidosJ2', 'ranking'),
                'order' => array('ranking' => 'DESC')));
        }else{
            //$this->Jugador->Behaviors->attach('Mongodb.SqlCompatible');
            $jugadores = $this->Jugador->find('all', array(
                'conditions' => array('genero' => $genero),
                'fields' => array('nombres', 'apellidos','ranking'),
                'order' => array('ranking' => 'DESC')));
        }
        $this->set('genero', $genero);
        $this->set('jugadores', $jugadores);
    }
}

?>
