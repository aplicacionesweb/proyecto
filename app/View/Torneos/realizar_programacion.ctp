<div class="grid_16"> 
    <div class="block" id="tables">  
        <div class="box" id="verGrupos">
             <?php if(count($nombreTorneos)>0): ?>
                <h2>
                    <a href="#" id="">Realizar la Programación de un Torneo</a>
                </h2>
                <br/>
                <?php
                    echo $this->Form->create('Torneo', array('action' => 'realizarProgramacion'));
                    echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo', 'id' => 'gran_torneo'));
                ?>
                <div id="end"><?php echo $this->Form->end('Programar'); ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer();?>