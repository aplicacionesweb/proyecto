<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">

            <h2>Detalle de datos del Torneo</h2>
            <br>

            <table>
                <tbody>
                    <tr>
                        <td>Nombre:</td>
                        <td><?php echo $torneo['GranTorneo']['nombre'] ?></td>
                    </tr>
                    <tr>
                        <td>Fecha de inicio:</td>
                        <td>
                            Día: <?php echo $torneo['GranTorneo']['fecha_inicio']['day'] ?>
                            Mes: <?php echo $torneo['GranTorneo']['fecha_inicio']['month'] ?>
                            Año: <?php echo $torneo['GranTorneo']['fecha_inicio']['year'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Duración:</td>
                        <td><?php echo $torneo['GranTorneo']['duracion'] ?> días</td>
                    </tr>
                    <tr>
                        <td>Costo de inscripción:</td>
                        <td><?php echo $torneo['GranTorneo']['costo_inscripcion'] ?></td>
                    </tr>
                    <tr>
                        <td>Canchas disponibles:</td>
                        <td><?php echo $torneo['GranTorneo']['canchas_disponibles'] ?></td>
                    </tr>
                    <tr>
                        <td>Cantiad máxima de jugadores admitidos:</td>
                        <td><?php echo $torneo['GranTorneo']['cantidad_maxima_jugadores'] ?></td>
                    </tr>
                    <tr>
                        <td>Cantidad de jugadores inscritos:</td>
                        <td><?php echo $torneo['GranTorneo']['jugadores_inscritos'] ?></td>
                    </tr>
                    <tr>
                        <td>Cupos disponibles:</td>
                        <td><?php echo $torneo['GranTorneo']['cantidad_maxima_jugadores'] - $torneo['GranTorneo']['jugadores_inscritos'] ?></td>
                    </tr>
                    <tr>
                        <td>Premios Categoría 1A y 1B</td>
                        <td>
                            Puesto 1: <?php echo $torneo['GranTorneo']['premios'][0]['valor'] ?> <br/>
                            Puesto 2: <?php echo $torneo['GranTorneo']['premios'][1]['valor'] ?> <br/>
                            Puesto 3: <?php echo $torneo['GranTorneo']['premios'][2]['valor'] ?> <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>Premios Categoría 2A y 2B</td>
                        <td>
                            Puesto 1: <?php echo $torneo['GranTorneo']['premios'][3]['valor'] ?> <br/>
                            Puesto 2: <?php echo $torneo['GranTorneo']['premios'][4]['valor'] ?> <br/>
                            Puesto 3: <?php echo $torneo['GranTorneo']['premios'][5]['valor'] ?> <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>Premios Categoría 3A y 3B</td>
                        <td>
                            Puesto 1: <?php echo $torneo['GranTorneo']['premios'][6]['valor'] ?> <br/>
                            Puesto 2: <?php echo $torneo['GranTorneo']['premios'][7]['valor'] ?> <br/>
                            Puesto 3: <?php echo $torneo['GranTorneo']['premios'][8]['valor'] ?> <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>Premios Categoría 4A y 4B</td>
                        <td>
                            Puesto 1: <?php echo $torneo['GranTorneo']['premios'][9]['valor'] ?> <br/>
                            Puesto 2: <?php echo $torneo['GranTorneo']['premios'][10]['valor'] ?> <br/>
                            Puesto 3: <?php echo $torneo['GranTorneo']['premios'][11]['valor'] ?> <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>Premios Categoría Novatos</td>
                        <td>
                            Puesto 1: <?php echo $torneo['GranTorneo']['premios'][12]['valor'] ?> <br/>
                            Puesto 2: <?php echo $torneo['GranTorneo']['premios'][13]['valor'] ?> <br/>
                            Puesto 3: <?php echo $torneo['GranTorneo']['premios'][14]['valor'] ?> <br/>
                        </td>
                    </tr>
                </tbody> 
            </table>
            <br>
            <h2>Jugadores Inscritos al Torneo</h2>
            <table>
                <caption>Participantes modalidad Solo</caption>
                <thead>
                    <tr>
                        <th>Identificación</th>
                        <th>Nombres</th>
                        <th>Género</th>
                        <th>Categoria</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($jugadores as $jugador): ?>
                        <tr>
                            <td><?php echo $jugador['Jugador']['identificacion'] ?></td>
                            <td><?php echo $jugador['Jugador']['nombres'] ?></td>
                            <td><?php echo $jugador['Jugador']['genero'] ?></td>
                            <td><?php echo $jugador['Jugador']['categoria'] ?></td>
                            <td><?php echo $this->Html->link('Detalle', array('controller' => 'jugadors', 'action' => 'detalle', $jugador['Jugador']['_id'])) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <table>
                <caption>Participantes modalidad Pareja</caption>
                <thead>
                    <tr>
                        <th>Identificación J1</th>
                        <th>Nombres J1</th>
                        <th>Identificación J2</th>
                        <th>Nombres J2</th>
                        <th>Categoria</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($parejas as $pareja): ?>
                        <tr>
                            <td><?php echo $pareja['Pareja']['identificacionJ1'] ?></td>
                            <td><?php echo $pareja['Pareja']['nombresJ1'] ?></td>
                            <td><?php echo $pareja['Pareja']['identificacionJ2'] ?></td>
                            <td><?php echo $pareja['Pareja']['nombresJ2'] ?></td>
                            <td><?php echo $pareja['Pareja']['categoria'] ?></td>
                            <td>
                                <?php echo $this->Html->link('Detalle', array('controller' => 'parejas', 'action' => 'detalle', $pareja['Pareja']['_id'])) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>