<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" id="tables" >

        <div class="box" id="listaTorneos">
            <h2>
                <a href="#" id="">Lista Torneos</a>
            </h2> 
            <br/>

            <table>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($torneos as $torneo): ?>
                        <tr>
                            <td><?php echo $torneo['GranTorneo']['nombre'] ?></td>
                            <td>
                                Día: <?php echo $torneo['GranTorneo']['fecha_inicio']['day'] ?>
                                Mes: <?php echo $torneo['GranTorneo']['fecha_inicio']['month'] ?>
                                Año: <?php echo $torneo['GranTorneo']['fecha_inicio']['year'] ?>
                            </td>
                            <td><?php echo $torneo['GranTorneo']['estado'] ?></td>
                            <td>
                                <?php echo $this->Html->link('Detalle', array('action' => 'detalle', $torneo['GranTorneo']['_id'])) ?> &nbsp;
                                <?php echo $this->Html->link('Editar', array('action' => 'editar', $torneo['GranTorneo']['_id'])) ?> &nbsp;
                                <?php
                                echo $this->Form->postLink(
                                        'Eliminar', array('action' => 'eliminar', $torneo['GranTorneo']['_id']), array('confirm' => 'Realmente está seguro?'))
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
