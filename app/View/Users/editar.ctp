<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">
            <h2>
                <a href="#" id="">Editar datos del Usuario</a>
            </h2>
            <br/>
            <br/>
            <fieldset class="form-users">
                <legend>Datos del Usuario</legend>
                <?php
                echo $this->Form->create('User', array('action' => 'editar'));
                echo $this->Form->hidden('_id');
                echo 'Tipo:  ' . $this->request->data['User']['tipo'];
                echo $this->Form->input('identificacion');
                echo $this->Form->input('nombres');
                echo $this->Form->input('apellidos');
                echo $this->Form->input('fecha_nacimiento', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento'));
                echo $this->Form->input('contrasena', array('type' => 'password', 'label' => 'Nueva Contraseña'));
                echo $this->Form->input('confirmar_contrasena', array('type' => 'password', 'label' => 'Confirma tu contraseña'));
                echo $this->Form->input('experiencia', array('label' => 'Experiencia en años (Solo para Jueces)'));
                echo $this->Form->end('Actualizar');
                ?>
            </fieldset>

        </div>
    </div>
</div>