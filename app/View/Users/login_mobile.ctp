<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>
        </title>
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.0/jquery.mobile-1.1.0.min.css" />
       
        <style>
            /* App custom styles */
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
        </script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.0/jquery.mobile-1.1.0.min.js">
        </script>        
        </script>
    </head>
    <body>


        <!-- Home -->
        <div data-role="page" id="page1">
            <div data-theme="a" data-role="header">
                <h3>
                    Ingreso Sistema
                </h3>
            </div>
            <div data-role="content" style="padding: 15px">
                <div style="width: 288px; height: 100px; position: relative; background-color: #fbfbfb; border: 1px solid #b8b8b8;">
                    <img src="http://codiqa.com/static/images/v2/image.png" alt="image" style="position: absolute; top: 50%; left: 50%; margin-left: -16px; margin-top: -18px" />
                </div>

            <?php echo $this->Session->flash('auth'); ?>
            <?php echo $this->Form->create('User'); ?>

                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <?php
                        //Campos de entrada
                        echo $this->Form->input('identificacion', array('label' => 'Identificación', 'id' => 'textinput1'));
                        ?>
                    </fieldset>
                </div>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup">
                        <?php echo $this->Form->input('contrasena', array('type' => 'password', 'label' => 'Contraseña', 'id' => 'textinput2'));
                        ?>
                    </fieldset>
                </div>
                 <?php echo $this->Form->submit('Ingresar'); ?>	
            </div>
        </div>
        <script>
            //App custom javascript
        </script>
    </body>
</html>