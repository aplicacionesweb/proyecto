<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" id="tables" >

        <div class="box" id="listarPartidos">
            <h2>
                <a href="#" id="">Listar Partidos</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Jugador');
            echo $this->Form->input('gran_torneo', array('type' => 'select',
                'options' => $nombresTorneos,
                'label' => 'Torneo', 'id' => 'torneo'));
            echo $this->Form->end();

            echo $this->Js->get('#torneo')->event('change', $this->Js->request(array('controller' => 'jugadors', 'action' => 'listar'), array(//'update' => '#success',
                        'update' => '#listaPartidos',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                        ))
                    )));
            ?>
            <br/><br/>
            <div id="listaJugadores">
                <?php include 'lista.ctp'; ?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Js->writeBuffer(); ?>