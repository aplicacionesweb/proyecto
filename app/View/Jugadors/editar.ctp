<div class="grid_16"> 
    <div class="block" >


        <div class="box" >

<h2>Editar datos del Jugador</h2>
<br/>
<fieldset class="form-jugador">
    <legend>Datos del Jugador</legend>
<?php
echo $this->Form->create('Jugador', array('action' => 'editar'));
echo $this->Form->hidden('_id');
echo $this->Form->input('identificacion');
echo $this->Form->input('nombres');
echo $this->Form->input('apellidos');
echo $this->Form->input('fecha_nacimiento', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento'));
echo $this->Form->input('genero', array('label' => 'Género', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
echo $this->Form->end('Guardar');
?>
</fieldset>
        </div>
        </div>
    </div>