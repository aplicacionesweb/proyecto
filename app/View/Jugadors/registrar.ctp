<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" >


        <div class="box" >
            <h2>
                <a href="#" id="">Registrar Jugador</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Jugador', array('action' => 'registrar', 'type' => 'post'));
            echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo'));
            //echo $this->Form->input('modalidad', array('type' => 'select', 'options' => array('Solo'=>'Solo','Pareja'=>'Parejas'), 'label' => 'Seleccione la modalidad', 'id' => 'modalidad'));
            ?>

            <fieldset class="form-jugador" >
                <legend>Datos del Jugador</legend>
                <?php echo $this->Form->input('identificacion', array('label' => 'Identificación', 'id' => 'ide', 'div' => 'input')); ?>
                <div id="participante">
                    <?php
                    echo $this->Form->hidden('_id');
                    echo $this->Form->input('nombres', array('div' => 'input'));
                    echo $this->Form->input('apellidos', array('div' => 'input'));
                    echo $this->Form->input('fecha_nacimiento', array('type' => 'date','div' => 'input', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento'));
                    echo $this->Form->input('genero', array('type' => 'select','div' => 'input', 'label' => 'Género',    'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
                    echo $this->Form->input('categoria', array('type' => 'select','div' => 'input', 'label' => 'Selecciona la categoría', 'options' => $categorias));
                    ?>
                </div>
            </fieldset>

            <div id="end">
                    <?php
                    if(!empty ($nombreTorneos)){
                        echo $this->Form->end('Registrar');
                    } 
                    ?>
            </div>

            <?php
            echo $this->Js->get('#ide')->event('blur', $this->Js->request(array('controller' => 'jugadors', 'action' => 'getDatosParticipante'), array(//'update' => '#success',
                        'update' => '#participante',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => true,
                            'inline' => true
                        ))
                    )));
            ?>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>

