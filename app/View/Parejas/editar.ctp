<div class="grid_16"> 
    <div class="block" >

        <div class="box" id="parejas-box">

            <h2>Editar datos de la Pareja</h2>

            <fieldset class="form-pareja">
                <br/>
                <?php
                echo $this->Form->create('Pareja', array('action' => 'editar'));
                echo $this->Form->hidden('_id');
                echo $this->Form->input('identificacionJ1', array('label' => 'Identificación primer jugador'));
                echo $this->Form->input('nombresJ1', array('label' => 'Nombres primer jugador'));
                echo $this->Form->input('apellidosJ1', array('label' => 'Apellidos primer jugador'));
                echo $this->Form->input('fecha_nacimientoJ1', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento primer jugador'));
                echo $this->Form->input('generoJ1', array('label' => 'Género primer jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
                echo $this->Form->input('identificacionJ2', array('label' => 'Identificación segundo jugador'));
                echo $this->Form->input('nombresJ2', array('label' => 'Nombres segundo jugador'));
                echo $this->Form->input('apellidosJ2', array('label' => 'Apellidos segundo jugador'));
                echo $this->Form->input('fecha_nacimientoJ2', array('type' => 'date', 'dateFormat' => 'DMY', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento segundo jugador'));
                echo $this->Form->input('generoJ2', array('label' => 'Género segundo jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
                echo $this->Form->end('Guardar');
                ?>


            </fieldset>

        </div>
    </div>
</div>