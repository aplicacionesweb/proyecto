<div class="grid_16"> 
    <div class="block" >
        <div class="box" id="users-box">


            <h2>Detalle de datos de la Pareja</h2>
            <br>

            <table>
                <tbody>
                    <tr>
                        <td>Identificación J1:</td>
                        <td><?php echo $pareja['Pareja']['identificacionJ1'] ?></td>
                    </tr>
                    <tr>
                        <td>Nombres J1:</td>
                        <td><?php echo $pareja['Pareja']['nombresJ1'] ?></td>
                    </tr>
                    <tr>
                        <td>Apellidos J1:</td>
                        <td><?php echo $pareja['Pareja']['apellidosJ1'] ?></td>
                    </tr>
                    <tr>
                        <td>Fecha de Nacimiento J1:</td>
                        <td>Día: <?php echo $pareja['Pareja']['fecha_nacimientoJ1']['day'] ?>
                            Mes: <?php echo $pareja['Pareja']['fecha_nacimientoJ1']['month'] ?>
                            Año: <?php echo $pareja['Pareja']['fecha_nacimientoJ1']['year'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Género J1:</td>
                        <td><?php echo $pareja['Pareja']['generoJ1'] ?></td>
                    </tr>
                    <tr>
                        <td>Identificación J2:</td>
                        <td><?php echo $pareja['Pareja']['identificacionJ2'] ?></td>
                    </tr>
                    <tr>
                        <td>Nombres J2:</td>
                        <td><?php echo $pareja['Pareja']['nombresJ2'] ?></td>
                    </tr>
                    <tr>
                        <td>Apellidos J2:</td>
                        <td><?php echo $pareja['Pareja']['apellidosJ2'] ?></td>
                    </tr>
                    <tr>
                        <td>Fecha de Nacimiento J2:</td>
                        <td>Día: <?php echo $pareja['Pareja']['fecha_nacimientoJ2']['day'] ?>
                            Mes: <?php echo $pareja['Pareja']['fecha_nacimientoJ2']['month'] ?>
                            Año: <?php echo $pareja['Pareja']['fecha_nacimientoJ2']['year'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Género J2:</td>
                        <td><?php echo $pareja['Pareja']['generoJ2'] ?></td>
                    </tr>
                    <tr>
                        <td>Categoria Actual:</td>
                        <td><?php echo $pareja['Pareja']['categoria'] ?></td>
                    </tr>
                    <tr>
                        <td>Ranking:</td>
                        <td><?php echo $pareja['Pareja']['ranking'] ?></td>
                    </tr>
                </tbody>
            </table>
            <?php
            ?>

        </div>
    </div>
</div>