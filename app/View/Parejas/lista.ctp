<table>
    <thead>
        <tr>
            <th>Identificación J1</th>
            <th>Nombres J1</th>
            <th>Identificación J2</th>
            <th>Nombres J2</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($parejas as $pareja): ?>
            <tr>
                <td><?php echo $pareja['Pareja']['identificacionJ1'] ?></td>
                <td><?php echo $pareja['Pareja']['nombresJ1'] ?></td>
                <td><?php echo $pareja['Pareja']['identificacionJ2'] ?></td>
                <td><?php echo $pareja['Pareja']['nombresJ2'] ?></td>
                <td>
                    <?php echo $this->Html->link('Detalle', array('action' => 'detalle', $pareja['Pareja']['_id'])) ?> &nbsp;
                    <?php echo $this->Html->link('Editar', array('action' => 'editar', $pareja['Pareja']['_id'])) ?>
                    <!--<?php
                echo $this->Form->postLink(
                        'Eliminar', array('action' => 'Eliminar', $jugador['Jugador']['_id']), array('confirm' => 'Realmente está seguro?'))
                    ?>-->
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
