<?php echo $this->Html->script('jquery', FALSE) ?>
<div class="grid_16"> 
    <div class="block" >

        <div class="box" id="parejas-box">
            <h2>
                <a href="#" id="">Registrar Jugador</a>
            </h2>
            <br/>

            <?php
            echo $this->Form->create('Pareja', array('action' => 'registrar', 'type' => 'post'));
            echo $this->Form->input('gran_torneo', array('type' => 'select', 'options' => $nombreTorneos, 'label' => 'Seleccione el Torneo'));
            //echo $this->Form->input('modalidad', array('type' => 'select', 'options' => array('Solo'=>'Solo','Pareja'=>'Parejas'), 'label' => 'Seleccione la modalidad', 'id' => 'modalidad'));
            ?>

            <fieldset class="form-pareja">
                <legend>Datos de la Pareja</legend>
                <?php echo $this->Form->input('identificacionJ1', array('label' => 'Identificación primer jugador', 'id' => 'ide1','div' => 'input')); ?>
                <?php echo $this->Form->input('identificacionJ2', array('label' => 'Identificación segundo jugador', 'id' => 'ide2','div' => 'input')); ?>
                <div id="participante">
                    <?php
                    echo $this->Form->hidden('_id');
                    echo $this->Form->input('nombresJ1', array('label' => 'Nombres primer jugador','div' => 'input'));
                    echo $this->Form->input('apellidosJ1', array('label' => 'Apellidos primer jugador','div' => 'input'));
                    echo $this->Form->input('fecha_nacimientoJ1', array('type' => 'date', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento primer jugador'));
                    echo $this->Form->input('generoJ1', array('label' => 'Género primer jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
                    echo $this->Form->input('nombresJ2', array('label' => 'Nombres segundo jugador','div' => 'input'));
                    echo $this->Form->input('apellidosJ2', array('label' => 'Apellidos segundo jugador','div' => 'input'));
                    echo $this->Form->input('fecha_nacimientoJ2', array('type' => 'date', 'maxYear' => date('Y') - 18, 'minYear' => date('Y') - 100, 'label' => 'Fecha de Nacimiento segundo jugador'));
                    echo $this->Form->input('generoJ2', array('label' => 'Género segundo jugador', 'type' => 'select', 'options' => array('Masculino' => 'Masculino', 'Femenino' => 'Femenino')));
                    echo $this->Form->input('categoria', array('type' => 'select', 'label' => 'Selecciona la categoría', 'options' => $categorias));
                    ?>
                </div>
            </fieldset>

            <div id="end"><?php echo $this->Form->end('Registrar'); ?></div>

            <?php
            echo $this->Js->get('#ide2')->event('blur', $this->Js->request(array('controller' => 'parejas', 'action' => 'getDatosParticipante'), array(
                        'update' => '#participante',
                        'method' => 'post',
                        'async' => true,
                        'dataExpression' => true,
                        'data' => $this->Js->serializeForm(array(
                            'isForm' => false,
                            'inline' => true
                                )
                            ))));
            ?>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>

