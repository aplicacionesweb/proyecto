<?php

    class Pareja extends AppModel
    {
        public $name = 'Pareja';
        public $primaryKey = '_id';
        
        public $validate = array(
            'nombresJ1' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el nombre(s) del jugador 1'
                )
            ),
            'nombresJ2' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el nombre(s) del jugador 2'
                )
            ),
            'apellidosJ1"' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el apellido(s) del jugador 1'
                )
            ),
            'apellidosJ2"' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Debe de proporcionar el apellido(s) del jugador 2'
                )
            ));
    }
?>
