granTorneo1 = {fecha_inicio : {month:'01',day:'15',year:'2012'},
               duracion : 5,
               consto_inscripcion: 150000,
               premios: [{categoria: '1A', valor: 80000, puesto: 1},
                         {categoria: '1A', valor: 50000, puesto: 2},
                         {categoria: '1A', valor: 20000, puesto: 3}],
               canchas_disponibles: 12,
               cantidad_max_jugadores: 25,
               nombre: 'Torneo univalle Enero 2012'}

granTorneo2 = {fecha_inicio : {month:'06',day:'21',year:'2012'},
               duracion : 5,
               consto_inscripcion: 150000,
               premios: [{categoria: '1A', valor: 80000, puesto: 1},
                         {categoria: '1A', valor: 50000, puesto: 2},
                         {categoria: '1A', valor: 20000, puesto: 3}],
               canchas_disponibles: 12,
               cantidad_max_jugadores: 25,
               nombre: 'Torneo univalle Junio 2012'}

db.gran_torneos.save(granTorneo1)
db.gran_torneos.save(granTorneo2)
